import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./utils/global.components.js";
import "./utils/global.plugins.js";
import $const from "./global.const.js";
Vue.prototype.$const = $const;
Vue.config.productionTip = false;

/**
 * SEO适配改造
 * @type {Vue | CombinedVueInstance<Vue, object, object, object, Record<never, any>>}
 */
const root = new Vue({
  router,
  store,
  render: (h) => h(App),
  mounted() {
    document.dispatchEvent(new Event("custom-render-trigger"));
  },
});

document.addEventListener("DOMContentLoaded", function () {
  root.$mount("#app");
});
