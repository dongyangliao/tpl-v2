const crypto = require("crypto");
const KEY = "X";
const TYPE = "aes-256-gcm";

/**
 * aes
 * @param word
 * @param aesKey
 * @returns {string}
 */
function encodeAes(word, aesKey = KEY) {
  // check au
  if (!word) return "";
  if (typeof word != "string") word = JSON.stringify(word);
  // doing
  const md5 = crypto.createHash("sha256");
  const result = md5.update(aesKey).digest();
  const iv = "0123456789ABCDEF";
  const cipher = crypto.createCipheriv(TYPE, result, iv);
  const encrypted = cipher.update(word, "utf8");
  const finalstr = cipher.final();
  const tag = cipher.getAuthTag();
  const res = Buffer.concat([encrypted, finalstr, tag]);
  return res.toString("base64");
}

export default {
  encodeAes,
};
