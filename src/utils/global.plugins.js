// // 全局插件得引入

import Vue from "vue";
const requirePlugin = require.context(
  // 其组件目录的相对路径
  "@/plugins",
  // 是否查询其子目录
  true,
  // 匹配基础组件文件名的正则表达式
  /config\.js$/
);
const allCmp = [];
requirePlugin.keys().forEach((fileName) => {
  const pluginConfig = requirePlugin(fileName).default;
  allCmp.push(pluginConfig);
});

const globalCmp = allCmp.filter((item) => {
  return item.global;
});
globalCmp.forEach((item) => {
  // 获取组件配置
  const pluginConfig = item.file;
  // 全局注册组件
  Vue.use(pluginConfig);
});
