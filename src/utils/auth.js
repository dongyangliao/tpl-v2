import Cookies from "js-cookie";

const TokenKey = "token";

export function getToken() {
  return Cookies.get(TokenKey);
}

export function setToken(token) {
  return Cookies.set(TokenKey, token);
}

export function removeToken() {
  return process.env.NODE_ENV !== "dev"
    ? Cookies.remove(TokenKey, {
        path: "/",
        domain: process.env.VUE_APP_COOKIE_DOMAIN,
      })
    : Cookies.remove(TokenKey);
}

// ++++++++++++++++++++++++++原项目中用于工业标识查询（具体未查）
const IndusRecords = "indusRecords";

export function getIndusRecords() {
  return Cookies.get(IndusRecords);
}

export function setIndusRecords(indusRecords) {
  return Cookies.set(IndusRecords, indusRecords);
}
