/**
 * 获取浏览器信息
 * @returns {{brand, version}|{brand: string, version: string}}
 */
export function getBrowersInfo() {
  let navigator = window.navigator;
  if (navigator.userAgent.indexOf("Edg") > -1) {
    if (navigator.userAgentData && navigator.userAgentData.brands) {
      let idx = navigator.userAgentData.brands.length - 1;
      let { brand, version } = navigator.userAgentData.brands[idx] || {
        brand: "unknown",
        version: "unknown",
      };
      return { brand, version };
    } else {
      return { brand: "Microsoft Edge", version: "version unknown" };
    }
  }
  if (navigator.userAgent.indexOf("QQBrowser") > -1) {
    let userAgentArray = navigator.userAgent.split(" ");
    let version = "version unknown";
    userAgentArray.map((item) => {
      if (item.indexOf("QQBrowser") > -1) {
        version = item.substr(10);
        let brand = "QQBrowser";
        return { brand, version };
      }
    });
    return { brand: "QQBrowser", version: version };
  }
  if (navigator.userAgent.indexOf("Firefox") > -1) {
    let userAgentArray = navigator.userAgent.split(" ");
    userAgentArray.forEach((item) => {
      if (item.indexOf("Firefox") > -1) {
        let version = item.substr(8);
        return { brand: "Firefox", version: version };
      }
    });
  }
  if (navigator.userAgent.indexOf("Chrome") > -1) {
    if (navigator.userAgentData && navigator.userAgentData.brands) {
      let idx = navigator.userAgentData.brands.length - 1;
      let { brand, version } = navigator.userAgentData.brands[idx];
      return { brand, version };
    } else {
      return { brand: "Google Chrome", version: "version unknown" };
    }
  }
  return { brand: "brower unknown", version: "version unknown" };
}

/**
 * 获取system信息
 * @returns {null|{system_name: string, system_version: string}}
 */
export function getSystem() {
  var version = window.navigator.userAgent;
  if (version.indexOf("Windows NT 5") != -1) {
    // console.log('这是XP系统');
    return { system_name: "Windows NT 5", system_version: "XP" };
  }
  if (version.indexOf("Windows NT 7") != -1) {
    // console.log('这是win7系统');
    return { system_name: "Windows NT 7", system_version: "7" };
  }
  if (version.indexOf("Windows NT 10") != -1) {
    // console.log('这是win10系统');
    return { system_name: "Windows NT 10", system_version: "10" };
  }
  if (version.indexOf("Android") != -1) {
    return { system_name: "Android", system_version: "all" };
  }
  if (version.indexOf("iPhone") != -1) {
    return { system_name: "iPhone", system_version: "all" };
  }
  return { system_name: "Unknown", system_version: "unknown" };
}
