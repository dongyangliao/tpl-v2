const seoKV = require("../../seo.config.js");
/**
 * 获取SEO配置，配合vue-meta
 * @param route
 * @param option
 * @returns {{keywords: string, description: string, title: string}|*|{title}}
 */
export const getSEO = (route, option = null) => {
  if (option && option.title) return option;
  if (seoKV[route]) return seoKV[route];
  return {
    title: "title default",
    keywords: "keywords default",
    description: "description default",
  };
};
