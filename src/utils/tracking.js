import FingerprintJS from "@fingerprintjs/fingerprintjs";

/**
 * tracking
 * @param code
 * @param param
 */
function tracking(code, param) {
  FingerprintJS.load().then((fp) => {
    fp.get().then((result) => {
      // visitorId 指纹
      const visitorId = result.visitorId;
      const dto = {
        eventCode: code,
        eventParam: param,
        userAgent: window.navigator.userAgent,
        visitorId: visitorId,
        //...
      };
      console.log(dto);
      // axios.post(...)
    });
  });
}

/**
 * 按钮事件（例子）
 * event_track_button()
 */
function event_track_button(el, obj) {
  let eventCode = obj.code;
  let eventParam = [
    {
      paramName: "current_url",
      paramValue: window.location.href,
    },
    {
      paramName: "button_name",
      paramValue: el.innerText,
    },
    {
      paramName: "page_name",
      paramValue: document.title,
    },
  ];
  tracking(eventCode, eventParam);
}

export default {
  event_track_button,
};
