// 注册全局组件
import Vue from "vue";
const requireComponent = require.context(
  // 其组件目录的相对路径
  "@/components",
  // 是否查询其子目录
  true,
  // 匹配基础组件文件名的正则表达式
  /config\.js$/
);
const allCmp = [];
requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName).default;
  allCmp.push(componentConfig);
});

const globalComponents = allCmp.filter((item) => {
  return item.global;
});

globalComponents.forEach((item) => {
  // 获取组件配置
  const componentConfig = item.file;
  // 获取组件的 PascalCase 命名
  const componentName = componentConfig.name;
  // 全局注册组件
  Vue.component(componentName, componentConfig);
});
