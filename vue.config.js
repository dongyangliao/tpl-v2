"use strict";
const path = require("path");
const PrerenderSPAPlugin = require("prerender-spa-plugin");
const JSDOMRenderer = require("@prerenderer/renderer-jsdom");
const productionGzipExtensions = ["js", "css"];
const CompressionPlugin = require("compression-webpack-plugin");
const webpack = require("webpack");
const SEO = require("./seo.config.js");

function resolve(dir) {
  return path.join(__dirname, dir);
}

/**
 * 写入SEO配置HTML (title description, keyword)
 * @param html
 * @param SEO
 * @param route
 * @returns {string|*}
 */
function addSeo(html, SEO, route) {
  const aSEO = SEO[route];
  if (!SEO[route]) return html;
  const kwMeta = `<meta name="keywords" content="${aSEO.keywords}">`;
  const despMeta = `<meta name="description" content="${aSEO.description}">`;
  let newHtml = html.replace(
    /<title>[^<]*<\/title>/i,
    "<title>" + aSEO.title + "</title>"
  );
  const addStart = newHtml.indexOf("</head>");
  newHtml =
    newHtml.slice(0, addStart) +
    "  " +
    kwMeta +
    "\n" +
    "    " +
    despMeta +
    "\n  " +
    newHtml.slice(addStart);
  return newHtml;
}

/**
 * 加入SEO的页面
 * @param SEO
 * @returns {string[]}
 */
function addSeoList(SEO) {
  console.log("\nSEO路由包括：", Object.keys(SEO));
  return Object.keys(SEO);
}

module.exports = {
  publicPath: process.env.VUE_APP_PATH,
  outputDir: "dist",
  assetsDir: "static",
  lintOnSave: process.env.NODE_ENV === "development",
  productionSourceMap: process.env.NODE_ENV !== "development",
  devServer: {
    open: true,
    overlay: {
      warnings: false,
      errors: true,
    },
    // proxy: process.env.VUE_APP_API + process.env.VUE_APP_GATEWAY,
  },
  configureWebpack: () => {
    return {
      plugins: [
        // SEO
        new PrerenderSPAPlugin({
          // 生成文件的路径，也可以与webpack打包的一致。下面这句话非常重要！！！
          // 这个目录只能有一级，如果目录层次大于一级，在生成的时候不会有任何错误提示，在预渲染的时候只会卡着不动。
          staticDir: path.join(__dirname, "dist"),
          // 对应自己的路由文件，比如a有参数，就需要写成 /a/param1。
          // routes: [
          //   '/',
          //   '/about'
          // ],
          routes: addSeoList(SEO),
          // 这个很重要，如果没有配置这段，也不会进行预编译
          renderer: new JSDOMRenderer({
            injectProperty: "__PRERENDER_INJECTED",
          }),
          postProcess(renderedRoute) {
            // renderedRoute.html = renderedRoute.html.replace(
            //   /<title>[^<]*<\/title>/i,
            //   "<title>" + titles[renderedRoute.route] + "</title>"
            // );
            renderedRoute.html = addSeo(
              renderedRoute.html,
              SEO,
              renderedRoute.route
            );
            return renderedRoute;
          },
        }),
      ],
    };
  },
  chainWebpack(config) {
    /**
     * webpack-bundle-analyzer
     */
    if (process.env.use_analyzer) {
      config
        .plugin("webpack-bundle-analyzer")
        .use(require("webpack-bundle-analyzer").BundleAnalyzerPlugin);
    }
    /**
     * moment locale
     */
    config
      .plugin("ignore")
      .use(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/));

    /**
     * 打包文件添加hash
     */
    config.output
      .filename("static/js/[name].[hash].js")
      .chunkFilename("static/js/[name].[hash].js")
      .end();

    config.plugins.delete("preload"); // TODO: need test
    config.plugins.delete("prefetch"); // TODO: need test
    config.output
      .filename("static/js/[name].[hash].js")
      .chunkFilename("static/js/[name].[hash].js")
      .end();
    // set preserveWhitespace
    config.module
      .rule("vue")
      .use("vue-loader")
      .loader("vue-loader")
      .tap((options) => {
        options.compilerOptions.preserveWhitespace = true;
        return options;
      })
      .end();

    /**
     * cheap-source-map
     */
    config
      // https://webpack.js.org/configuration/devtool/#development
      .when(process.env.NODE_ENV === "development", (config) =>
        config.devtool("cheap-source-map")
      );

    config.when(process.env.NODE_ENV !== "development", (config) => {
      config
        .plugin("ScriptExtHtmlWebpackPlugin")
        .after("html")
        .use("script-ext-html-webpack-plugin", [
          {
            // `runtime` must same as runtimeChunk name. default is `runtime`
            inline: /runtime\..*\.js$/,
          },
        ])
        .end();
      config.plugin("compressionPlugin").use(
        new CompressionPlugin({
          algorithm: "gzip",
          test: new RegExp("\\.(" + productionGzipExtensions.join("|") + ")$"),
          threshold: 10240,
          minRatio: 0.8,
        })
      );
      config.optimization.splitChunks({
        chunks: "all",
        cacheGroups: {
          libs: {
            name: "chunk-libs",
            test: /[\\/]node_modules[\\/]/,
            priority: 10,
            chunks: "initial", // only package third parties that are initially dependent
          },
          commons: {
            name: "chunk-commons",
            test: resolve("src/components"), // can customize your rules
            minChunks: 3, //  minimum common number
            priority: 5,
            reuseExistingChunk: true,
          },
        },
      });
      config.optimization.runtimeChunk("single");
    });
  },
};
